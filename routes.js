const http = require('http')

// Creates a variable 'port' to store the port number
const port = 4004

// Creates a variable 'server' that stores the output of the 'createServer' method
const server = http.createServer((request, response) => {

	// Accessing the 'greetings' route returns a message of 'Hellow Again World!'
	if (request.url == '/greetings'){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Hellow Again World!')
		console.log('Success!')

	// Accessing the 'homepage' route returns a mesage of 'This is the homepage'
	} else if (request.url == '/homepage'){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('This is the homepage.')
	
	// All other routes will return a message of "Page not available"
	} else {

		// Set a status code for the response - a 404 means Not Found
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Page is not available')
	}
})

// Uses the "server" and "port" variable created above.
server.listen(port);

// When server is running, console will the print the message:
console.log(`Server now accessible at localhost:${port}.`)
