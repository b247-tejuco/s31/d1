// use the "require" directive to load Node.js modules
// the messages sent by the "client", usually a web browser, are called "request"
// likewise, msgs sent by 'server' = 'response'

// the http module lets nodejs transfer data using the hhypertext trasnfer protocl
// using this module's createServer() method, we can create an HTTP server that listens to
//  to request on a specified portn and provides responses back to the client.
let http = require("http");

// the http module has a createServer() method that acepts a function as an argument
// the function argument has parameters for rceiving req and sending responses

http.createServer(function(request, response) {

	// We will use the writeHead() method:
		// to set a status code for the - a 200 means
		// set a content-type of the response as a plain text msg
	response.writeHead(200, {'Content-Type': 'text/plain'});

	// send the response with text content 'hellow world'
	response.end('Hellow Wurld?!');
}).listen(4000)

// when server is running, console will print the message:
console.log('Server running at localhost:4000');